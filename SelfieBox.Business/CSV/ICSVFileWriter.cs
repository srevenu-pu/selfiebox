﻿namespace SelfieBox.Business.CSV
{
    public interface ICsvFileWriter
    {
        void Write(string filename, string content);
    }
}
﻿using SelfieBox.Business.DataBase;
using SelfieBox.Model;

namespace SelfieBox.Business.CSV
{
    public class CsvExporting : BusinessBase<EmailEntity>
    {
        private static string GenerateFirstLineOfPhotoEntity()
        {
            var firstLine = "";

            firstLine += "Identifiant de la photo,";
            firstLine += "Identificant GUID de la photo,";
            firstLine += "Date de la photo,";
            firstLine += "Nom de la photo,";
            firstLine += "Email de l'utilisateur";
            firstLine += "\r\n";

            return firstLine;
        }

        public string ExportPhotoEntityToCsv()
        {
            var contentCsv = GenerateFirstLineOfPhotoEntity();

            var data = GetItems();

            foreach (var item in data)
            {
                contentCsv += item.Id + ",";
                contentCsv += item.IdGuid + ",";
                contentCsv += item.PhotoEntity.PhotoDate + ",";
                contentCsv += item.PhotoEntity.Image + ",";
                contentCsv += item.Email;
                contentCsv += "\r\n";
            }

            return contentCsv;
        }
    }
}
﻿using System.Collections.Generic;

namespace SelfieBox.Business.DataBase
{
    public interface IBusiness<T>
    {
        T Get(int id);
        List<T> GetItems();
        void InsertItem(T item, bool withChildren);
        void InsertItems(IEnumerable<T> items, bool withChildren);
    }
}
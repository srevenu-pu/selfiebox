﻿using System.Collections.Generic;
using SelfieBox.DataAccess;
using SelfieBox.Model;

namespace SelfieBox.Business.DataBase
{
    public class BusinessBase<T> : IBusiness<T> where T : EntityBase, new()
    {
        private readonly DatabaseAccess<T> _dbAccess = new DatabaseAccess<T>();

        public T Get(int id)
        {
            return _dbAccess.GetItem(id);
        }

        public List<T> GetItems()
        {
            return _dbAccess.GetItems();
        }

        public void InsertItem(T item, bool withChildren)
        {
            _dbAccess.InsertItem(item, withChildren);
        }

        public void InsertItems(IEnumerable<T> items, bool withChildren)
        {
            _dbAccess.InsertItems(items, withChildren);
        }

        public void InsertItemWithChildren(T item)
        {
        }

        public DatabaseAccess<T> GetDbAccess()
        {
            return _dbAccess;
        }
    }
}
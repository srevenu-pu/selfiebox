using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Views;

namespace SelfieBox.Droid.Views
{
    [Activity(Label = "View for Step5ConfirmationView", ScreenOrientation = ScreenOrientation.Landscape)]
    public class Step5ConfirmationView : MvxActivity
    {
        private Button _btnOpenFolder;

        protected override void OnCreate(Bundle bundle)
        {
            ActionBar.Hide();
            Window.AddFlags(WindowManagerFlags.Fullscreen);

            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Step5ConfirmationView);

            _btnOpenFolder = FindViewById<Button>(Resource.Id.BtnOpenFolder);
            _btnOpenFolder.Click += _btnOpenFolder_Click;
        }

        private void _btnOpenFolder_Click(object sender, System.EventArgs e)
        {
            Intent intent = new Intent();
            var uri = Android.Net.Uri.Parse(Environment.ExternalStorageDirectory.AbsolutePath + "/SelfieBox/");
            intent.SetData(uri);
            intent.SetType("*/*");
            intent.SetAction(Intent.ActionView);
            StartActivity(Intent.CreateChooser(intent, "Choose an application to open the folder:"));
            
        }
    }
}
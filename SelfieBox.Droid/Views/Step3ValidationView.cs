using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Views;

namespace SelfieBox.Droid.Views
{
    [Activity(Label = "View for Step3ValidationView", ScreenOrientation = ScreenOrientation.Landscape)]
    public class Step3ValidationView : MvxActivity
    { 
        protected override void OnCreate(Bundle bundle)
        {
            ActionBar.Hide();
            Window.AddFlags(WindowManagerFlags.Fullscreen);

            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Step3ValidationView);
        }
    }
}
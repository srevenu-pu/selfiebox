using System;
using System.IO;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Content.Res;
using Android.OS;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Java.IO;
using MvvmCross.Binding.Droid.Views;
using MvvmCross.Droid.Views;

namespace SelfieBox.Droid.Views
{
    [Activity(Label = "View for Step4EnvoiView", WindowSoftInputMode = SoftInput.StateAlwaysVisible, ScreenOrientation = ScreenOrientation.Landscape)]
    public class Step4EnvoiView : MvxActivity
    {
        private Button _btnAddMail, _btnSeeConditions;
        private EditText _editTxtMail;
        private InputMethodManager _imm;
        private ListView _listEmails;

        protected override void OnCreate(Bundle bundle)
        {
            ActionBar.Hide();
            Window.AddFlags(WindowManagerFlags.Fullscreen);

            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Step4EnvoiView);

            _editTxtMail = (EditText)FindViewById(Resource.Id.EditTxtEmailStep4);
            _editTxtMail.TextChanged += _editTxtMail_TextChanged;

            _imm = (InputMethodManager)GetSystemService(InputMethodService);

            _btnAddMail = (Button)FindViewById(Resource.Id.BtnAddMailStep4);
            _btnAddMail.Click += _btnAddMail_Click;

            _btnSeeConditions = (Button)FindViewById(Resource.Id.BtnSeeConditions);

            _listEmails = (MvxListView)FindViewById(Resource.Id.ListEmailsStep4);
            _listEmails.ItemClick += ListEmailsOnItemClick;



            string textConditions = "";

            try
            {
                var input = Assets.Open("conditions.txt");
                StreamReader sr = new StreamReader(input);
                textConditions = sr.ReadToEnd();
            }
            catch (Exception e)
            {

            }


            AlertDialog alertDialog = new AlertDialog.Builder(this).Create();
            alertDialog.SetMessage(textConditions);
            alertDialog.SetTitle("Selfie Box conditions:");
            alertDialog.SetButton("Close", (sender, args) =>
            {
                {
                    alertDialog.Dismiss();
                }
            });


            _btnSeeConditions.Click += (sender, e) =>
            {
                alertDialog.Show();
            };

        }

        private void _editTxtMail_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            _btnAddMail.Enabled = _editTxtMail.Text.Length != 0;
        }

        private void ListEmailsOnItemClick(object sender, AdapterView.ItemClickEventArgs itemClickEventArgs)
        {
            _imm.HideSoftInputFromWindow(_editTxtMail.WindowToken, 0);
        }

        private void _btnAddMail_Click(object sender, EventArgs e)
        {
            _imm.HideSoftInputFromWindow(_editTxtMail.WindowToken, 0);
            _editTxtMail.Text = "";
        }
    }
}
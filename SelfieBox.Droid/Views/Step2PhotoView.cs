using System;
using System.IO;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Hardware;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using Java.IO;
using Java.Lang;
using Java.Text;
using Java.Util;
using MvvmCross.Droid.Views;
using SelfieBox.Core.ViewModels;
using Camera = Android.Hardware.Camera;
using Environment = Android.OS.Environment;
using Exception = System.Exception;
using File = Java.IO.File;
using FileNotFoundException = Java.IO.FileNotFoundException;
using IOException = Java.IO.IOException;

namespace SelfieBox.Droid.Views
{
    [Activity(Label = "View for Step2PhotoView", ScreenOrientation = ScreenOrientation.Landscape)]
    public class Step2PhotoView : MvxActivity, ISurfaceHolderCallback, Camera.IPictureCallback
    {
        private const string EXT_PICTURES = ".JPEG";
        private const string PREFIX_PICTURES = "IMG_";
        private const string TIMESTAMP_PICTURES = "yyyyMMdd_HHmmss";

        private Button _btnLaunchTimer;

        private Camera _camera;
        private int _cameraId;
        private Context _context;
        private bool _hasCamera;
        private ImageView _imageFlash;
        private int _pictureId;
        private ISurfaceHolder _surfaceHolder;
        private SurfaceView _surfaceView;
        private TextView _textViewTimer;

        #region Surface Changed

        public void SurfaceChanged(ISurfaceHolder holder, [GeneratedEnum] Format format, int width, int height)
        {
            if (_camera != null)
            {
                PrepareCamera();
            }
            else
            {
                _cameraId = GetFrontCameraId();
                GetCameraInstance();
            }
        }

        #endregion

        #region Surface Created

        public void SurfaceCreated(ISurfaceHolder holder)
        {
            try
            {
                if (_camera != null)
                {
                    ReleaseCamera();
                }

                if (HasCamera())
                {
                    GetCameraInstance();
                }
            }
            catch (Exception e)
            {
                Log.Debug("CameraController", "failed to open camera : " + e);
            }
        }

        #endregion

        #region Surface Destroyed

        public void SurfaceDestroyed(ISurfaceHolder holder)
        {
            ReleaseCamera();
        }

        #endregion

        #region Camera Release

        public void ReleaseCamera()
        {
            if (_camera != null)
            {
                _camera.StopPreview();
                _camera.Release();
                _camera = null;
            }
        }

        #endregion

        #region Get the front camera id

        /// <summary>
        ///     Get the front camera id
        /// </summary>
        /// <returns>Return the front camera Id</returns>
        private int GetFrontCameraId()
        {
            var cameraId = -1;
            var cameraInfo = new Camera.CameraInfo();

            for (var i = 0; i < Camera.NumberOfCameras; i++)
            {
                Camera.GetCameraInfo(i, cameraInfo);

                if (cameraInfo.Facing == CameraFacing.Front)
                {
                    cameraId = i;
                    break;
                }
            }

            if (_cameraId != -1)
                _hasCamera = true;
            else
                _hasCamera = false;

            return cameraId;
        }

        #endregion

        #region HasCamera

        /// <summary>
        ///     Get if the device has camera
        /// </summary>
        /// <returns>Return if the device has camera</returns>
        private bool HasCamera()
        {
            return _hasCamera;
        }

        #endregion

        #region Get the camera instance

        /// <summary>
        ///     Get the camera Instance
        /// </summary>
        /// <returns>Return the camera object</returns>
        private Camera GetCameraInstance()
        {
            _camera = null;

            if (HasCamera())
            {
                try
                {
                    _camera = Camera.Open(_cameraId);
                    PrepareCamera();
                }
                catch (Java.Lang.Exception e)
                {
                    _hasCamera = false;
                    Log.Debug("CameraController", "Camera error: " + e.Message);
                }
            }

            return _camera;
        }

        #endregion

        #region Prepare My Camera

        /// <summary>
        ///     Set my JPEG Quality and save
        /// </summary>
        private void PrepareCamera()
        {
            try
            {
                _camera.SetPreviewDisplay(_surfaceHolder);
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }

            var parameters = _camera.GetParameters();
            parameters.JpegQuality = 100;

            //Set my surfaceView
            var pictureSize = parameters.PictureSize;
            var layoutParam = _surfaceView.LayoutParameters;

            layoutParam.Height = pictureSize.Height;
            layoutParam.Width = pictureSize.Width;

            _camera.SetParameters(parameters);


            _camera.StartPreview();
        }

        #endregion

        #region Place the mark

        private bool PlaceMark(string imagePath)
        {
            var retour = false;

            var bmOptions = new BitmapFactory.Options();
            var src = BitmapFactory.DecodeFile(imagePath, bmOptions);
            var w = src.Width;
            var h = src.Height;
            var result = Bitmap.CreateBitmap(w, h, src.GetConfig());
            var canvas = new Canvas(result);
            canvas.DrawBitmap(src, 0, 0, null);
            var waterMark = BitmapFactory.DecodeResource(BaseContext.Resources, Resource.Drawable.logotodisplay);
            canvas.DrawBitmap(waterMark, 0, 0, null);


            FileStream stream = null;
            try
            {
                stream = new FileStream(imagePath, FileMode.Open);
                result.Compress(Bitmap.CompressFormat.Jpeg, 100, stream); // bmp is your Bitmap instance
                retour = true;
            }
            catch (Java.Lang.Exception e)
            {
                e.PrintStackTrace();
                retour = false;
            }
            finally
            {
                try
                {
                    stream?.Close();
                }
                catch (IOException e)
                {
                    e.PrintStackTrace();
                    retour = false;
                }
            }

            return retour;
        }

        #endregion

        #region Reset and Go To Step 3

        private void ResetAndGoToStep3()
        {
            _btnLaunchTimer.ClearAnimation();
            _imageFlash.ClearAnimation();
            _btnLaunchTimer.Enabled = true;

            (ViewModel as Step2PhotoViewModel).CloseMeAndGoStep3Command.Execute();
        }

        #endregion

        #region Base OnCreate

        protected override void OnCreate(Bundle bundle)
        {
            ActionBar.Hide();
            Window.AddFlags(WindowManagerFlags.Fullscreen);

            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Step2PhotoView);

            _context = this;

            _surfaceView = FindViewById<SurfaceView>(Resource.Id.SurfaceCamera);
            _surfaceHolder = _surfaceView.Holder;
            _surfaceHolder.AddCallback(this);
            _surfaceHolder.SetType(SurfaceType.PushBuffers);


            _cameraId = GetFrontCameraId();

            _btnLaunchTimer = FindViewById<Button>(Resource.Id.BtnLaunchTimer);
            _btnLaunchTimer.Click += _btnLaunchTimer_Click;

            _textViewTimer = FindViewById<TextView>(Resource.Id.TextTimer);

            _imageFlash = FindViewById<ImageView>(Resource.Id.ImageFlash);

            (ViewModel as Step2PhotoViewModel).TakePicture += Step2PhotoView_TakePicture;
            (ViewModel as Step2PhotoViewModel).FadeOutText += Step2PhotoView_FadeOutText;
        }

        private void Step2PhotoView_FadeOutText()
        {
            RunOnUiThread(new Runnable(AnimateMyText));
        }

        private void AnimateMyText()
        {
            _textViewTimer.StartAnimation(new AlphaAnimation(1.0f, 0.0f) { Duration = 1000, FillAfter = true });
        }

        private void _btnLaunchTimer_Click(object sender, EventArgs e)
        {
            var _fadeOutAnimation = new AlphaAnimation(1.0f, 0.0f)
            {
                Duration = 1000,
                FillAfter = true
            };
            _btnLaunchTimer.StartAnimation(_fadeOutAnimation);
            _fadeOutAnimation.AnimationEnd += _fadeOutAnimation_AnimationEnd;
        }

        private void _fadeOutAnimation_AnimationEnd(object sender, Animation.AnimationEndEventArgs e)
        {
            _btnLaunchTimer.Enabled = false;
        }

        #endregion

        #region Take picture

        private void Step2PhotoView_TakePicture()
        {
            RunOnUiThread(new Runnable(AnimateMyScreen));
        }

        private void AnimateMyScreen()
        {
            var imageFlashInAnimation = new AlphaAnimation(0.0f, 1.0f)
            {
                Duration = 500
            };
            _imageFlash.StartAnimation(imageFlashInAnimation);
            imageFlashInAnimation.AnimationEnd += ImageFlashInAnimation_AnimationEnd;
        }

        private void ImageFlashInAnimation_AnimationEnd(object sender, Animation.AnimationEndEventArgs e)
        {
            _camera.TakePicture(null, null, this);
        }

        #region GetOutputMediaFile

        private File GetOutputMediaFile()
        {
            var dir = new File(Environment.ExternalStorageDirectory.AbsolutePath + "/SelfieBox/");

            // Create the storage directory if it does not exist
            if (!dir.Exists())
            {
                if (!dir.Mkdirs())
                {
                    return null;
                }
            }

            // Create a media file name
            var timeStamp = new SimpleDateFormat(TIMESTAMP_PICTURES).Format(new Date());

            File mediaFile;

            if (_pictureId > -1)
            {
                mediaFile =
                    new File(dir.Path + File.Separator + PREFIX_PICTURES + timeStamp + "_" + _pictureId + EXT_PICTURES);
            }
            else
            {
                mediaFile = new File(dir.Path + File.Separator + PREFIX_PICTURES + timeStamp + EXT_PICTURES);
            }

            return mediaFile;
        }

        #endregion

        #region CallBack OnPictureTaken

        public void OnPictureTaken(byte[] data, Camera camera)
        {
            var pictureFile = GetOutputMediaFile();

            if (pictureFile == null)
            {
                Log.Debug("CameraController", "Error creating media file, check storage permissions");
                return;
            }

            try
            {
                Log.Debug("CameraController", "File created");
                var fos = new FileOutputStream(pictureFile);
                fos.Write(data);
                fos.Close();

                var success = PlaceMark(pictureFile.AbsolutePath);
                //success = Mark(pictureFile.getAbsolutePath());
            }
            catch (FileNotFoundException e)
            {
                Log.Debug("CameraController", "File not found: " + e.Message);
            }
            catch (IOException e)
            {
                Log.Debug("CameraController", "Error accessing file: " + e.Message);
            }

            (ViewModel as Step2PhotoViewModel).ImagePath = pictureFile.AbsolutePath;
            ResetAndGoToStep3();
        }

        #endregion

        #endregion
    }
}
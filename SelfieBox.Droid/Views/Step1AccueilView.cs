using System.IO;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Views;

namespace SelfieBox.Droid.Views
{
    [Activity(Label = "View for Step1AccueilView", ScreenOrientation = ScreenOrientation.Landscape)]
    public class Step1AccueilView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            ActionBar.Hide();
            Window.AddFlags(WindowManagerFlags.Fullscreen);

            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Step1AccueilView);  
        }
    }
}
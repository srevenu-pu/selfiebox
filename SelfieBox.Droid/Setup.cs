using Android.Content;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Platform;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;
using MvvmCross.Platform.Plugins;
using SelfieBox.Business.CSV;
using SelfieBox.Core;
using SelfieBox.DataAccess;
using SelfieBox.Droid.Modules;

namespace SelfieBox.Droid
{
    public class Setup : MvxAndroidSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            return new App();
        }

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

        protected override void InitializeApp(IMvxPluginManager pluginManager)
        {
            base.InitializeApp(pluginManager);
            Mvx.RegisterSingleton<ISqLite>(() => new SqLiteConn());
            Mvx.RegisterSingleton<ICsvFileWriter>(() => new CsvFileWriter());
        }
    }
}
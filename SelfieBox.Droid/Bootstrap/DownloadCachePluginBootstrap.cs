using MvvmCross.Platform.Plugins;
using MvvmCross.Plugins.DownloadCache;

namespace SelfieBox.Droid.Bootstrap
{
    public class DownloadCachePluginBootstrap
        : MvxPluginBootstrapAction<PluginLoader>
    {
    }
}
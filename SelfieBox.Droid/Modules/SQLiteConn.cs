using System.IO;
using Android.OS;
using SelfieBox.DataAccess;
using SQLite.Net;
using SQLite.Net.Platform.XamarinAndroid;
using File = Java.IO.File;

namespace SelfieBox.Droid.Modules
{
    public class SqLiteConn : ISqLite
    {
        SQLiteConnection ISqLite.GetConnection()
        {
            const string sqliteFilename = "SelfieBox.db3";
            var documentsPath = new File(Environment.ExternalStorageDirectory.AbsolutePath + "/SelfieBox/");
            if (!documentsPath.Exists())
                documentsPath.Mkdirs();

            var path = Path.Combine(documentsPath.ToString(), sqliteFilename);
            // Create the connection
            var conn = new SQLiteConnection(new SQLitePlatformAndroid(), path, false);
            // Return the database connection
            return conn;
        }
    }
}
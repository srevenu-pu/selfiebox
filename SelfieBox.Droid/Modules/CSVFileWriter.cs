using System.IO;
using Android.OS;
using SelfieBox.Business.CSV;
using File = Java.IO.File;

namespace SelfieBox.Droid.Modules
{
    public class CsvFileWriter : ICsvFileWriter
    {
        public void Write(string filename, string content)
        {
            // Check if the appdirectoryExist if not create it
            var dir = new File(Environment.ExternalStorageDirectory.AbsolutePath + "/SelfieBox/");
            if (!dir.Exists())
                dir.Mkdirs();

            using (var writer = new StreamWriter(dir.AbsolutePath + "/" + filename, false))
            {
                writer.Write(content);
            }
        }
    }
}
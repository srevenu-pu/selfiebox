﻿using MvvmCross.Core.ViewModels;
using SelfieBox.Core.Services;

namespace SelfieBox.Core.ViewModels
{
    public class Step2PhotoViewModel : MvxViewModel
    {
        private int _imagesTaken;

        #region CTOR

        public Step2PhotoViewModel()
        {
            WantToDisplayTimer = false;
            ImageFlashVisible = false;
            TextButtonImageLeft = "Start the countdown!\r\n(3 left)";
        }

        #endregion

        #region Method => Reset for back

        private void ResetForBack()
        {
            WantToDisplayTimer = false;
            ImageFlashVisible = false;
        }

        #endregion

        #region Event

        public event TakePictureEvent TakePicture;
        public event FadeOutTextEvent FadeOutText;

        #endregion

        #region Delegate 

        public delegate void TakePictureEvent();

        public delegate void FadeOutTextEvent();

        #endregion

        #region MvxProp => ImageFlash Visible

        private bool _imageFlashVisible;

        public bool ImageFlashVisible
        {
            get { return _imageFlashVisible; }
            set
            {
                _imageFlashVisible = value;
                RaisePropertyChanged(() => ImageFlashVisible);
            }
        }

        #endregion

        #region MvxProp => ImagePath of the picture

        private string _imagePath;

        public string ImagePath
        {
            get { return _imagePath; }
            set
            {
                _imagePath = value;
                RaisePropertyChanged(() => ImagePath);
            }
        }

        #endregion

        #region MvxProp => Want to Display Timer

        private bool _wantToDisplayTimer;

        public bool WantToDisplayTimer
        {
            get { return _wantToDisplayTimer; }
            set
            {
                _wantToDisplayTimer = value;
                RaisePropertyChanged(() => WantToDisplayTimer);
            }
        }

        #endregion

        #region MvxProp => Value Of Timer

        private int _valueOfTimer;

        public int ValueOfTimer
        {
            get { return _valueOfTimer; }
            set
            {
                _valueOfTimer = value;
                if (_valueOfTimer != 0)
                {
                    WantToDisplayTimer = true;
                }
                else
                {
                    WantToDisplayTimer = false;
                    ImageFlashVisible = true;
                }
                RaisePropertyChanged(() => ValueOfTimer);
            }
        }

        #endregion

        #region MvxProp => Text Button image Left

        private string _textButtonImageLeft;

        public string TextButtonImageLeft
        {
            get { return _textButtonImageLeft; }
            set
            {
                _textButtonImageLeft = value;
                RaisePropertyChanged(() => TextButtonImageLeft);
            }
        }

        #endregion

        #region MvxCommand => Close and Go Step1 (Accueil)

        private MvxCommand _closeAndGoStep1Command;

        public IMvxCommand CloseAndGoStep1Command
        {
            get
            {
                _closeAndGoStep1Command = _closeAndGoStep1Command ?? new MvxCommand(DoCloseAndGoStep1Command);
                return _closeAndGoStep1Command;
            }
        }

        private void DoCloseAndGoStep1Command()
        {
            ShowViewModel<Step1AccueilViewModel>();
            Close(this);
        }

        #endregion

        #region MvxCommand => Display Timer 

        private MvxCommand _displayTimer;

        public IMvxCommand DisplayTimerCommand
        {
            get
            {
                _displayTimer = _displayTimer ?? new MvxCommand(DoStartTimerCommand);
                return _displayTimer;
            }
        }

        private async void DoStartTimerCommand()
        {
            var timer = new TimerCustom(1);
            timer.TimeElapsed += (s, t) => ValueOfTimer = t;
            await timer.StartAsync(() => { TakePicture(); }, () => { FadeOutText(); });
        }

        #endregion

        #region MvxCommand => Close me and Go to Step3 (Validation View) 

        private MvxCommand _closeMeAndGoStep3Command;

        public IMvxCommand CloseMeAndGoStep3Command
        {
            get
            {
                _closeMeAndGoStep3Command = _closeMeAndGoStep3Command ?? new MvxCommand(DoCloseMeAndGoStep3Command);
                return _closeMeAndGoStep3Command;
            }
        }

        private void DoCloseMeAndGoStep3Command()
        {
            _imagesTaken++;
            var imageLeft = 3 - _imagesTaken;

            TextButtonImageLeft = "Start the countdown!\r\n(" + imageLeft + " left)";

            if (imageLeft == 0)
            {
                ShowViewModel<Step3ValidationViewModel>(new {imagePath = ImagePath, goBack = false});
                Close(this);
            }
            else
            {
                ResetForBack();
                ShowViewModel<Step3ValidationViewModel>(new {imagePath = ImagePath, goBack = true});
            }
        }

        #endregion
    }
}
using MvvmCross.Core.ViewModels;

namespace SelfieBox.Core.ViewModels
{
    public class Step1AccueilViewModel : MvxViewModel
    {
        #region MvxCommand => Go to Step2 (Photo View) 

        private MvxCommand _goToStep2PhotoViewCommand;

        public IMvxCommand GoToStep2PhotoViewCommand
        {
            get
            {
                _goToStep2PhotoViewCommand = _goToStep2PhotoViewCommand ?? new MvxCommand(DoGoToStep2PhotoViewCommand);
                return _goToStep2PhotoViewCommand;
            }
        }

        private void DoGoToStep2PhotoViewCommand()
        {
            ShowViewModel<Step2PhotoViewModel>(); 
            Close(this);
        }

        #endregion 
    }
}
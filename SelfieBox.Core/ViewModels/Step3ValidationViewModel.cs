﻿using System;
using Java.IO;
using MvvmCross.Core.ViewModels;

namespace SelfieBox.Core.ViewModels
{
    public class Step3ValidationViewModel : MvxViewModel
    {
        #region Init ViewModel

        public void Init(string imagePath, bool goBack)
        {
            ImagePath = imagePath;
            ButtonGoBackEnable = goBack;
        }

        #endregion

        #region MvxProp => ImagePath of the picture

        private string _imagePath;

        public string ImagePath
        {
            get { return _imagePath; }
            set
            {
                _imagePath = value;
                RaisePropertyChanged(() => ImagePath);
            }
        }

        #endregion

        #region MvxProp => Enable Go Back Button

        private bool _buttonGoBackEnable;

        public bool ButtonGoBackEnable
        {
            get { return _buttonGoBackEnable; }
            set
            {
                _buttonGoBackEnable = value;
                RaisePropertyChanged(() => ButtonGoBackEnable);
            }
        }

        #endregion

        #region MvxCommand => Go to Step2 (Close this window and back)

        private MvxCommand _closeAndBackCommand;

        public IMvxCommand CloseAndBackCommand
        {
            get
            {
                _closeAndBackCommand = _closeAndBackCommand ?? new MvxCommand(DoCloseAndBackCommand);
                return _closeAndBackCommand;
            }
        }

        private void DoCloseAndBackCommand()
        {
            var file = new File(ImagePath);
            file.Delete();
            Close(this);
        }

        #endregion

        #region MvxCommand => Close and go to Step4 (Envoi)

        private MvxCommand _closeAndGoStep4Command;

        public IMvxCommand CloseAndGoStep4Command
        {
            get
            {
                _closeAndGoStep4Command = _closeAndGoStep4Command ?? new MvxCommand(DoCloseAndGoStep4Command);
                return _closeAndGoStep4Command;
            }
        }

        private void DoCloseAndGoStep4Command()
        {
            ShowViewModel<Step4EnvoiViewModel>(new {imagePath = ImagePath});
            Close(this);
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.OS;
using Android.Content;
using MvvmCross.Core.ViewModels;

namespace SelfieBox.Core.ViewModels
{
    public class Step5ConfirmationViewModel : MvxViewModel
    {
        #region MvxCommand => Go to Step1 (Accueil View)

        private MvxCommand _goToStep1AccueilViewCommand;

        public IMvxCommand GoToStep1AccueilViewCommand 
        {
            get
            {
                _goToStep1AccueilViewCommand = _goToStep1AccueilViewCommand ?? new MvxCommand(DoGoToStep1AccueilViewCommand);
                return _goToStep1AccueilViewCommand;
            }
        }

        private void DoGoToStep1AccueilViewCommand()
        {
            Close(this);
            ShowViewModel<Step1AccueilViewModel>();
        }

        #endregion 
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Java.Util.Regex;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.UI;
using SelfieBox.Business.CSV;
using SelfieBox.Business.DataBase;
using SelfieBox.Model;

namespace SelfieBox.Core.ViewModels
{
    public class Step4EnvoiViewModel : MvxViewModel
    {
        private readonly ICsvFileWriter _csvFileWriter;
        private string _imageName;

        #region CTOR

        public Step4EnvoiViewModel()
        {
            _csvFileWriter = Mvx.Resolve<ICsvFileWriter>();
            Emails = new ObservableCollection<string>();
            EnableSenderButton = EnableDeleteButton = false;
            EnableAddButton = true;
            SetDisplayText("You can add 4 mails more", true, MvxColors.White);
        }

        #endregion

        #region Prop => Textbox email

        public string TextEmail { get; set; }

        #endregion

        #region Prop => Selected Item Index

        public int SelectedItemIndex { get; set; }

        #endregion 

        #region Init ViewModel

        public void Init(string imagePath)
        {
            try
            {
                var debut = imagePath.IndexOf(@"/IMG_");
                _imageName = imagePath.Substring(debut, imagePath.Length - debut);
            }
            catch (Exception expException)
            {
            }
        }

        #endregion

        #region Method => Set Text error/information

        private void SetDisplayText(string text, bool display, MvxColor color)
        {
            TextErrorMailText = text;
            TextErrorMailVisible = display;
            TextErrorMailColor = color;
        }

        #endregion

        #region Method => Export To CSV

        private void ExportToCsv()
        {
            var csvExporting = new CsvExporting();
            _csvFileWriter.Write("Test.csv", csvExporting.ExportPhotoEntityToCsv());
        }

        #endregion

        #region Method => This mail is valid

        public bool IsEmailValid(string email)
        {
            var regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

            var inputStr = email;

            var pattern = Pattern.Compile(regExpn, Pattern.CaseInsensitive);
            var matcher = pattern.Matcher(inputStr);

            if (matcher.Matches())
                return true;
            return false;
        }

        #endregion

        #region MvxProp => Condition Checked

        private bool _conditionChecked;

        public bool ConditionChecked
        {
            get { return _conditionChecked; }
            set
            {
                _conditionChecked = value;
                RaisePropertyChanged(() => ConditionChecked);
            }
        }

        #endregion

        #region MvxProp => Enable Sender Button

        private bool _enableSenderButton;

        public bool EnableSenderButton
        {
            get { return _enableSenderButton; }
            set
            {
                _enableSenderButton = value;
                RaisePropertyChanged(() => EnableSenderButton);
            }
        }

        #endregion

        #region MvxProp => Enable Add Button

        private bool _enableAddButton;

        public bool EnableAddButton
        {
            get { return _enableAddButton; }
            set
            {
                _enableAddButton = value;
                RaisePropertyChanged(() => EnableAddButton);
            }
        }

        #endregion

        #region MvxProp => Enable Delete Button

        private bool _enableDeleteButton;

        public bool EnableDeleteButton
        {
            get { return _enableDeleteButton; }
            set
            {
                _enableDeleteButton = value;
                RaisePropertyChanged(() => EnableDeleteButton);
            }
        }

        #endregion

        #region MvxProp => List of Emails

        private ObservableCollection<string> _emails;

        public ObservableCollection<string> Emails
        {
            get { return _emails; }
            set
            {
                _emails = value;
                RaisePropertyChanged(() => Emails);
            }
        }

        #endregion

        #region MvxProp => Text Mail Error/Information Visible

        private bool _textErrorMailVisible;

        public bool TextErrorMailVisible
        {
            get { return _textErrorMailVisible; }
            set
            {
                _textErrorMailVisible = value;
                RaisePropertyChanged(() => TextErrorMailVisible);
            }
        }

        #endregion

        #region MvxProp => Text Mail Error/Information Text

        private string _textErrorMailText;

        public string TextErrorMailText
        {
            get { return _textErrorMailText; }
            set
            {
                _textErrorMailText = value;
                RaisePropertyChanged(() => TextErrorMailText);
            }
        }

        #endregion

        #region MvxProp => Text Mail Error/Information Color

        private MvxColor _textErrorMailColor;

        public MvxColor TextErrorMailColor
        {
            get { return _textErrorMailColor; }
            set
            {
                _textErrorMailColor = value;
                RaisePropertyChanged(() => TextErrorMailColor);
            }
        }

        #endregion

        #region MvxCommand => Add email in list

        private MvxCommand _addMailCommand;

        public IMvxCommand AddMailCommand
        {
            get
            {
                _addMailCommand = _addMailCommand ?? new MvxCommand(DoAddMailCommand);
                return _addMailCommand;
            }
        }

        private void DoAddMailCommand()
        {
            if (!string.IsNullOrEmpty(TextEmail))
            {
                if (IsEmailValid(TextEmail))
                {
                    if (ConditionChecked)
                        EnableSenderButton = true;

                    Emails.Add(TextEmail);

                    if (Emails.Count >= 4)
                    {
                        SetDisplayText("You cannot add any more email address!", true, MvxColors.Red);
                        EnableAddButton = false;
                    }
                    else
                    {
                        SetDisplayText("You can add " + (4 - Emails.Count) + " mails more", true, MvxColors.White);
                    }
                }
                else
                {
                    SetDisplayText("This email isn't correct!", true, MvxColors.Red);
                }
            }
        }

        #endregion

        #region MvxCommand => Select email in list

        private MvxCommand _selectedEmailCommand;

        public IMvxCommand SelectedEmailCommand
        {
            get
            {
                _selectedEmailCommand = _selectedEmailCommand ?? new MvxCommand(DoSelectedEmailCommand);
                return _selectedEmailCommand;
            }
        }

        private void DoSelectedEmailCommand()
        {
            EnableDeleteButton = true;
        }

        #endregion

        #region MvxCommand => Delete Selected Item

        private MvxCommand _deleteSelectedItemCommand;

        public IMvxCommand DeleteSelectedItemCommand
        {
            get
            {
                _deleteSelectedItemCommand = _deleteSelectedItemCommand ?? new MvxCommand(DoDeleteSelectedItemCommand);
                return _deleteSelectedItemCommand;
            }
        }

        private void DoDeleteSelectedItemCommand()
        {
            try
            {
                if (SelectedItemIndex != -1)
                {
                    Emails.RemoveAt(SelectedItemIndex);
                    SelectedItemIndex = -1;

                    if (Emails.Count == 0)
                    {
                        EnableSenderButton = false;
                    }
                    else if (Emails.Count < 4)
                    {
                        EnableAddButton = true;
                    }
                }
            }
            catch (Exception)
            {
            }

            if (SelectedItemIndex == -1)
                EnableDeleteButton = false;

            SetDisplayText("You can add " + (4 - Emails.Count) + " mails more", true, MvxColors.White);
        }

        #endregion

        #region MvxCommand => Conditions Checked

        private MvxCommand _conditionCheckedCommand;

        public IMvxCommand ConditionCheckedCommand
        {
            get
            {
                _conditionCheckedCommand = _conditionCheckedCommand ?? new MvxCommand(DoConditionCheckedCommand);
                return _conditionCheckedCommand;
            }
        }

        private void DoConditionCheckedCommand()
        {
            var isChecked = ConditionChecked;

            if (isChecked && Emails.Count > 0)
                EnableSenderButton = true;
            else
                EnableSenderButton = false;
        }

        #endregion

        #region MvxCommand => Insert in BDD

        private MvxCommand _clickToInsertCommand;

        public IMvxCommand ClickToInsertCommand
        {
            get
            {
                _clickToInsertCommand = _clickToInsertCommand ?? new MvxCommand(DoInsertBddCommand);
                return _clickToInsertCommand;
            }
        }

        private void DoInsertBddCommand()
        {
            var guidForId = Guid.NewGuid().ToString();
            var dateTime = DateTime.Now.ToString();

            //var photoEntity = new PhotoEntity
            //{
            //    Image = "image",
            //    PhotoDate = dateTime,
            //    IdGuid = "guidid",
            //    Emails = null
            //};
            new PhotoBusiness();
            new EmailBusiness();

            var photo = new PhotoEntity();
            var emails = new List<EmailEntity>();


            foreach (var item in Emails)
            {
                var emailEntity = new EmailEntity
                {
                    IdGuid = guidForId,
                    Email = item,
                    PhotoEntity = photo
                };
                emails.Add(emailEntity);
                //new EmailBusiness().InsertItem(emailEntity, true);
            }

            photo.Emails = emails;
            photo.IdGuid = guidForId;
            photo.Image = _imageName;
            photo.PhotoDate = dateTime;

            new PhotoBusiness().InsertItem(photo, true);

            ExportToCsv();

            ShowViewModel<Step5ConfirmationViewModel>();
        }

        #endregion

        #region MvxCommand => Close and Go Step1 (Accueil)

        private MvxCommand _closeAndGoStep1Command;

        public IMvxCommand CloseAndGoStep1Command
        {
            get
            {
                _closeAndGoStep1Command = _closeAndGoStep1Command ?? new MvxCommand(DoCloseAndGoStep1Command);
                return _closeAndGoStep1Command;
            }
        }

        private void DoCloseAndGoStep1Command()
        {
            ShowViewModel<Step1AccueilViewModel>();
            Close(this);
        }

        #endregion
    }
}
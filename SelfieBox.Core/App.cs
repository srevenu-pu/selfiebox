using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.IoC;
using SelfieBox.Core.ViewModels;

namespace SelfieBox.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            RegisterAppStart<Step1AccueilViewModel>();
        }
    }
}
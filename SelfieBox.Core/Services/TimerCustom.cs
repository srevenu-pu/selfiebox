﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace SelfieBox.Core.Services
{
    public class TimerCustom
    {
        private bool _started;
        private readonly int _timeBase;

        public TimerCustom(int startTime)
        {
            Time = _timeBase = startTime;
        }

        public int Time { get; private set; }
        public event EventHandler<int> TimeElapsed;


        public async Task StartAsync(Action completeAction, Action fadeOutAction,
            CancellationToken token = default(CancellationToken))
        {
            if (_started) return;

            _started = true;


            while (_started)
            {
                // wait 1000 ms
                await Task.Delay(1000, token).ConfigureAwait(false);

                fadeOutAction.Invoke();

                if (Time == 0)
                {
                    //Timer finished
                    _started = false;
                    completeAction.Invoke();
                }

                TimeElapsed?.Invoke(this, Time);

                Time--;
            }
            Time = _timeBase;
        }
    }
}
﻿using System.Collections.Generic;
using MvvmCross.Platform;
using SelfieBox.Model;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;

namespace SelfieBox.DataAccess
{
    public class DatabaseAccess<T> : IDatabaseAccess<T> where T : EntityBase, new()
    {
        protected SQLiteConnection db;

        public DatabaseAccess()
        {
            db = Mvx.Resolve<ISqLite>().GetConnection();
            db.CreateTable<T>();
        }

        public List<T> GetItems()
        {
            // return db.Table<T>().ToList();
            return db.GetAllWithChildren<T>();
        }

        public T GetItem(int id)
        {
            return db.Get<T>(id);
        }

        #region InsertItem

        public void InsertItem(T item, bool withChildren = false)
        {
            if (withChildren)
            {
                db.InsertWithChildren(item, true);
            }
            else
            {
                db.Insert(item);
            }
        }

        #endregion

        #region InsertItems

        public void InsertItems(IEnumerable<T> items, bool withChildren = false)
        {
            foreach (var item in items)
            {
                InsertItem(item, withChildren);
            }
        }

        #endregion
    }
}
﻿using System.Collections.Generic;

namespace SelfieBox.DataAccess
{
    public interface IDatabaseAccess<T>
    {
        T GetItem(int id);
        List<T> GetItems();
        void InsertItem(T item, bool withChildren);
        void InsertItems(IEnumerable<T> items, bool withChildren);
    }
}
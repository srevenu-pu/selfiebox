﻿using SQLite.Net;

namespace SelfieBox.DataAccess
{
    public interface ISqLite
    {
        SQLiteConnection GetConnection();
    }
}
﻿namespace SelfieBox.Model
{
    public class EntityBase
    {
        public virtual int Id { get; set; }
    }
}
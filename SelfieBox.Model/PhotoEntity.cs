﻿using System.Collections.Generic;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace SelfieBox.Model
{
    public class PhotoEntity : EntityBase
    {
        [PrimaryKey]
        // public override int Id { get; set; }
        public string IdGuid { get; set; }

        public string PhotoDate { get; set; }

        public string Image { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<EmailEntity> Emails { get; set; }
    }
}
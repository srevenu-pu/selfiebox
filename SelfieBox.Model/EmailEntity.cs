﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace SelfieBox.Model
{
    public class EmailEntity : EntityBase
    {
        [PrimaryKey, AutoIncrement]
        public override int Id { get; set; }

        [ForeignKey(typeof (PhotoEntity))]
        public string IdGuid { get; set; }

        public string Email { get; set; }

        [ManyToOne]
        public PhotoEntity PhotoEntity { get; set; }
    }
}